# opm - Optional Package Manager

## Installation

ccli comes with opm preinstalled from https://gitlab.com/ruedigerp/cli_module_opm
You can upgrade your installation with `ccli opm install opm` or even use a different repository `ccli opm install opm different-repository`

## Configuration

Currently opm supports two options `show_url` and `show_version` which affects the output of `opm.list`

## Sources

You have a `sources` directory in your ccli configuration folder. These will be read by opm to provide packages from.

Every file within the sources directory will be read. For a source file to be valid consider the following syntax

```
repo_name    repo_url    repo_description
```

## Example repositories

By default opm comes with no repositories configured, you can use the following two examples as a starting point

> `${CCLI_CONF}/sources/rpr.source`

```
rpr https://gitlab.com/snippets/1901399/raw ruedigerp's repository
```

> `${CCLI_CONF}/sources/ima.source`

```
ima https://gitlab.com/iilonmasc/ccli-packages/-/raw/master/package.sources iilonmasc's package repository
```