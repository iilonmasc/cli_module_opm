import os, sys, errno
import zipfile
import shutil
import pathlib
import glob
from pathlib import Path
import requests
from requests.exceptions import SSLError
import json
import time

configuration = {}
module_name='opm'
module_version = '0.6.0'
commands = ['help', 'update', 'list', 'install', 'remove', 'clean', 'version']
config_section = module_name.lower()
command_help={
    'version': 'show version info',
    'help': 'show module help'
    }  # FIXME: add info for all commands
package_sources = {}

def execute(command, params, app_config):
    global configuration
    configuration = app_config
    if command == 'update':
        opm_update()
    elif command == 'list':
        opm_list()
    elif command == 'install':
        opm_install(params)
    elif command == 'clean':
        opm_clean(params)
    elif command == 'remove':
        opm_remove(params)
    elif command == 'help':
        opm_help()
    elif command == 'version':
        opm_version()

def opm_version():
    print ('Version: ', module_version)

def opm_update():
    if os.path.exists(configuration['sources']) and len(os.listdir(f'{configuration["sources"]}')) > 0:
        for source_file in os.listdir(f'{configuration["sources"]}'):
            with open(f'{configuration["sources"]}/{source_file}', 'r') as sf_handler:
                    contents = sf_handler.readlines()
            for line in contents:
                if len(line)>0:
                    package_source = line.split(' ')
                    package_sources[package_source[0]] = {'url': package_source[1], 'name': ' '.join(package_source[2:]).replace('\n','')}
        for index in package_sources:
            repository = package_sources.get(index)
            print(f'Checking repository {repository.get("name")}...', end=' ')
            package_file = f'{configuration["cache"]}/packages_{index}.json'
            req = requests.get(repository.get('url'))
            with open(package_file, 'wb') as json_file:
                json_file.write(req.content)
            data = req.json()
            print(f'found {len(data)} packages')
    else:
        print(f'No source file found, please add one in {configuration["sources"]}')

def opm_list():
    table_header = f'{"Package":15} | '
    table_division = f'{"-"*15} | '
    if configuration.get(config_section) and configuration[config_section].get('show_version'): 
        table_header += f'{"Version":10} | '
        table_division += f'{"-"*10} | '

    table_header += f'{"Description":70} | {"Source list":15} |'
    table_division += f'{"-"*70} | {"-"*15} |'
    if configuration.get(config_section) and configuration[config_section].get('show_url'): 
        table_header += f' {"Url":75} |'
        table_division += f' {"-"*75} |'

    print(table_header)
    print(table_division)

    for package_cache in glob.glob(f'{configuration["cache"]}/packages_*'):
        with open(package_cache) as json_file:
            data = json.load(json_file)

        for item in data:
            entry_output = f'{item.get("packagename"):15} | '
            if configuration.get(config_section) and configuration[config_section].get('show_version'): 
                entry_output += f'{item.get("version"):10} | '
            entry_output += f'{item.get("description"):70} | {os.path.basename(package_cache).split(".")[0]:15} |'
            if configuration.get(config_section) and configuration[config_section].get('show_url'): 
                entry_output += f' {item.get("url"):75} |'
            print(entry_output)

def opm_install(params):
    repositories = {}
    if len(params) > 0:
        for package_cache in glob.glob(f'{configuration["cache"]}/packages_*'):
            with open(package_cache) as json_file:
                data = json.load(json_file)
                repository = package_cache.split('/')[-1].split('.')[0][9:] # /home/user/.../packages_rpr_test.json > rpr_test
                repositories[repository] = {"repo_name": repository}
            for item in data:
                repositories[repository][item.get('packagename')] = {'name': item.get('packagename'), 'url': item.get('url'), 'archive': item.get('zipfile'), 'version': item.get('version')}
        if len(params) == 1:
            # no repo specified, fetch from first source available
            for repo in repositories:
                repo = repositories[repo]
                if params[0] in repo:
                    try:
                        install_package(params[0], repo)
                    except SSLError as e:
                        print(f'Unable to fetch Package {params[0]} from repo {repo.get("repo_name")}\n\t{e}')
                else:
                    print(f'Package {params[0]} not found in repo {repo.get("repo_name")}')
        else:
            if repositories.get(params[1]):
                repo = repositories[params[1]]
                if params[0] in repo:
                    install_package(params[0], repo)
                else:
                    print(f'Package {params[0]} not found in repo {repo}')
            else:
                print(f'Repo {params[1]} not found')
    else:
        print('No package specified')

def opm_remove(params):
    destdir = f'{configuration["modules"]}/{params[0]}'
    ## Try to remove tree; if failed show an error using try...except on screen
    try:
        shutil.rmtree(destdir)
        print ('Paket wurde deinstalliert.')
    except OSError as e:
        print ('Paket ist nicht installiert.')
        print (f'Error: {e.filename} - {e.strerror}.')
def opm_clean(params):
    print(f'Cleaning cache directory {configuration["cache"]}')
    for cache_file in glob.glob(f'{configuration["cache"]}/*'):
        if os.path.isdir(cache_file):
            shutil.rmtree(cache_file, ignore_errors=True)
        else:
            os.remove(cache_file)

# FIXME: translate and use correct help text
def opm_help():
    print ('Example Modul als Beispiel.')
    print ('Konfiguration für das Module anlegen, anzeigen und im Modul benutzen.\n')
    print ('Konfiguration anlegen:')
    print ('  hmgcli config_exmaple create')
    print ('Es werden alle Config Keys die in configkeys abgefragt.')
    print ('Config Section ist der Modulename in der INI Datei.\n')
    print ('Kondiguration anzeigen:')
    print ('  hmgcli config_example show\n\n')
    sys.exit()

def install_package(package, repository):
    item = repository.get(package)
    print(f'Downloading from repo {repository.get("repo_name")}: {item.get("name")}-{item.get("version")}')
    destfile = f'{configuration["cache"]}/{item.get("name")}.zip' 
    req = requests.get(f'{item.get("url")}/{item.get("archive")}.zip')
    with open(destfile, 'wb') as download_file:
        download_file.write(req.content)
    with zipfile.ZipFile(destfile,'r') as zip_ref:
        zip_ref.extractall(configuration["modules"])
    shutil.rmtree( f'{configuration["modules"]}/{item.get("name")}',ignore_errors=True)
    os.rename( f'{configuration["modules"]}/{item.get("archive")}', f'{configuration["modules"]}/{item.get("name")}')
    print(f'Package {item.get("name")} installed')
